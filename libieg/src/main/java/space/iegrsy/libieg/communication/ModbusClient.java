package space.iegrsy.libieg.communication;


import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

public class ModbusClient {
    private static final String TAG = ModbusClient.class.getSimpleName();

    // Default values
    private static final byte[] transactionIdentifier = toByteArray(1);
    private static final byte[] protocolIdentifier = toByteArray(0);
    private static final byte[] length = toByteArray(6);
    private static final byte unitIdentifier = 0x01;
    private static final byte functionCode = 0x03;

    private String host;
    private int port;

    private Socket tcpClientSocket;
    private InputStream inStream;
    private DataOutputStream outStream;
    private int connectTimeout = 10000; // 10ms

    public ModbusClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void Connect() throws IOException {
        Log.i(TAG, "Connecting: " + host + ":" + port);
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        tcpClientSocket = new Socket();
        tcpClientSocket.connect(socketAddress, connectTimeout);
        tcpClientSocket.setKeepAlive(true);
        tcpClientSocket.setSoTimeout(connectTimeout);
        outStream = new DataOutputStream(tcpClientSocket.getOutputStream());
        inStream = tcpClientSocket.getInputStream();
    }

    public void Disconnect() throws IOException {
        Log.i(TAG, "Disconnect");
        if (inStream != null)
            inStream.close();
        if (outStream != null)
            outStream.close();
        if (tcpClientSocket != null)
            tcpClientSocket.close();

        tcpClientSocket = null;
    }

    public int[] readHoldingRegisters(int startAddress, int quantity) throws Exception {
        if (tcpClientSocket == null || !isConnected())
            throw new Exception("Connection error.");
        if (startAddress > 65535 || quantity > 125)
            throw new Exception("Starting address must be 0 - 65535; quantity must be 0 - 125");

        byte[] startByte = toByteArray(startAddress);
        byte[] count = toByteArray(quantity);

        byte[] queryByte = new byte[]{
                transactionIdentifier[1],
                transactionIdentifier[0],
                protocolIdentifier[1],
                protocolIdentifier[0],
                length[1],
                length[0],
                unitIdentifier,
                functionCode,
                startByte[1],
                startByte[0],
                count[1],
                count[0]
        };

        outStream.write(queryByte);
        outStream.flush();

        byte[] charArray = new byte[8 * 1024];
        int numCharsRead;
        numCharsRead = inStream.read(charArray);
        if (numCharsRead < 9 + quantity * 2)
            throw new Exception("Wrong format of response.");

        return bytes2Integer(charArray, quantity);
    }

    private static int[] bytes2Integer(byte[] data, int quantity) {
        int[] response = new int[quantity];
        for (int i = 0; i < quantity; i++) {
            byte[] bytes = new byte[2];
            bytes[0] = data[9 + i * 2];
            bytes[1] = data[9 + i * 2 + 1];
            ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

            response[i] = byteBuffer.getShort();
        }
        return response;
    }

    private static byte[] toByteArray(int value) {
        byte[] result = new byte[]{(byte) value, (byte) (value >> 8)};
        return result;
    }

    public boolean isConnected() {
        return tcpClientSocket != null && tcpClientSocket.isConnected();
    }
}
