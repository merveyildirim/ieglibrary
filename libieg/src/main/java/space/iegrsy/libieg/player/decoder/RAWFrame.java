package space.iegrsy.libieg.player.decoder;

public class RAWFrame {
    public int id;
    public byte[] frameData;
    public long ts;

    public RAWFrame(int id) {
        this.id = id;
    }
}
