package space.iegrsy.ieglibrary;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.pedro.vlc.VlcListener;
import com.pedro.vlc.VlcVideoLibrary;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VlcTestActivity extends AppCompatActivity implements VlcListener {

    @BindView(R.id.vlc_surfaceView)
    SurfaceView surfaceView;

    @BindView(R.id.vlc_et_endpoint)
    TextInputEditText endPoint;

    @BindView(R.id.vlc_btn_start_stop)
    Button btnStartStop;

    private VlcVideoLibrary vlcVideoLibrary;

    private String[] options = new String[]{":fullscreen"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_vlc_test);

        ButterKnife.bind(this);

        vlcVideoLibrary = new VlcVideoLibrary(this, this, surfaceView);
        vlcVideoLibrary.setOptions(Arrays.asList(options));
    }

    @Override
    public void onComplete() {
        Toast.makeText(this, "Playing", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError() {
        Toast.makeText(this, "Error, make sure your endpoint is correct", Toast.LENGTH_SHORT).show();
        vlcVideoLibrary.stop();
        btnStartStop.setText(getString(R.string.start_player));
    }

    @Override
    public void onBuffering(org.videolan.libvlc.MediaPlayer.Event event) {

    }

    @OnClick(R.id.vlc_btn_start_stop)
    void togglePlayer() {
        if (!vlcVideoLibrary.isPlaying()) {
            vlcVideoLibrary.play(endPoint.getText().toString());
            btnStartStop.setText(getString(R.string.stop_player));
        } else {
            vlcVideoLibrary.stop();
            btnStartStop.setText(getString(R.string.start_player));
        }
    }
}